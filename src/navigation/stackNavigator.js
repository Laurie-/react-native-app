import * as React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import MainScreen from '../screens/MainScreen'
import DetailsScreen from '../screens/DetailsScreen';
import CalculatorScreen from '../screens/CalculatorScreen';

const Stack = createNativeStackNavigator();

const StackNavigator = () => {
    return(
        <Stack.Navigator>
            <Stack.Screen name="Home" component={MainScreen} />
            <Stack.Screen name="Details" component={DetailsScreen} />
            <Stack.Screen name="Calculator" component={CalculatorScreen} />
        </Stack.Navigator>
    )
}

export default StackNavigator;