import React from 'react';
import {Text, View, Linking} from 'react-native';


const DetailsScreen = ({route}) => {
    //console.log(JSON.stringify(props, null, 2))
  return (
    <View>
        <Text>nom : {route.params.name.name}</Text>
        <Text onPress={()=>{Linking.openURL(`tel:${route.params.phone.phone}`)}}>{route.params.phone.phone}</Text>
    </View>
  )
  
};

export default DetailsScreen;