import React, { useState } from 'react';
import {Text, View, Button} from 'react-native';

const CalculatorScreen = () => {
    const [count, setCount] = useState(0);
  return(
      <View>
          <Button title={'-'} onPress={() => setCount(count - 1)}/>
        <Text>{count}</Text>
          <Button title={'+'} onPress={() => setCount(count + 1)}/>
      </View>
  );
};

export default CalculatorScreen;