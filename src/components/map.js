import { enableLatestRenderer, Polyline } from 'react-native-maps';
import React, { useEffect, useState } from 'react';
import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import { View, StyleSheet } from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import axios from 'axios';

enableLatestRenderer();
let arrayUsers = [];

const allUsers = axios({
    method: 'get',
    url: 'https://jsonplaceholder.typicode.com/users',
}).then((response) => {
    //console.log(JSON.stringify(response.data, null, 2))
    response.data.map(function (currentItem, index) {
        arrayUsers.push(currentItem)
        //console.log(arrayUsers);
    });
});

//let records = [];


const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    map: {
        ...StyleSheet.absoluteFillObject,
    },
});


const handleLocation = (setUserLocation) => {
    Geolocation.getCurrentPosition(position => {
        //console.log(position);
        setUserLocation(position.coords)
    });
}


const Map = () => {
    const [userLocation, setUserLocation] = useState(null);
    const [mapRegion, setMapRegion] = useState({ latitude: 37.78825, longitude: -122.4324 })
    const [recordList, setRecordList] = useState(null);
    //const [coordinates, setCoordinates] = useState([]);

    useEffect(() => {
        userLocation &&
            setMapRegion({
                ...mapRegion,
                latitude: userLocation.latitude,
                longitude: userLocation.longitude
            })
    }, [userLocation]);


    useEffect(() => {
        axios({
            method: 'get',
            url: 'https://opendata.isere.fr/api/records/1.0/search/?dataset=plan-departemental-des-itineraires-de-promenades-et-de-randonnees-departement-de&q=&rows=2000&facet=id&facet=gestionnaire_nom&refine.gestionnaire_nom=Grenoble-Alpes+M%C3%A9tropole',
        }).then((response) => {
            console.log(JSON.stringify(response.data.records[0].fields.geo_shape.coordinates[0]));
            setRecordList(
                response.data.records.map((record) => {
                    return (record.fields.geo_shape.coordinates[0].map((point, index) => {
                        //console.log(point);
                        let coords = { longitude: point[0], latitude: point[1] };
                        //coordinates.push(coords);
                        return coords;
                    }))
                }))
        });
    }, [])

    return (
        <View style={styles.container}>
            <MapView
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={styles.map}
                showsUserLocation
                onMapLoaded={() => handleLocation(setUserLocation)}
                region={{
                    latitude: mapRegion.latitude,
                    longitude: mapRegion.longitude,
                    latitudeDelta: 0.015,
                    longitudeDelta: 0.0121,
                }}
            >
                {/* {arrayUsers.map(function(user, index) {
             return(
                 null
    //     <MapView.Marker
    //     coordinate={{latitude: parseInt(user.address.geo.lat),
    //     longitude: parseInt(user.address.geo.lng)}}
    //     title={user.name}
    //     description={user.username} 
    //     key={index}
    //  />
      )})} */}
                {recordList &&
                    recordList?.length &&
                    recordList.map((rec, index) => {
                        if (rec)
                            return (
                                <Polyline
                                key={index}
                                    coordinates={rec}
                                    strokeColor="#000" // fallback for when `strokeColors` is not supported by the map-provider
                                    strokeColors={[
                                        '#7F0000',
                                        '#00000000', // no color, creates a "long" gradient between the previous and next coordinate
                                        '#B24112',
                                        '#E5845C',
                                        '#238C23',
                                        '#7F0000'
                                    ]}
                                    strokeWidth={1}
                                />
                            )
                    })}

            </MapView>
        </View>
    )
};

export default Map