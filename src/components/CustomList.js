import React from 'react';
import {FlatList, View, Text, StyleSheet, Button, TouchableOpacity} from 'react-native';
import DATA from '../assets/users.json'
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

/*const DATA = [
    {
        firstName : 'Patrick',
        lastName : 'Dupont',
    },
    {
        firstName : 'Lucie',
        lastName : 'Morel',
    },
];*/

const ItemList = ({name, phone, navigation}) => {
    return (
        <TouchableOpacity
        onPress={() => navigation.navigate('Details', {name: {name}, phone:{phone}})}
        >
        <View style = {local.itemContainer}>
            <View style = {local.identityContainer}>
                <Text>{name}</Text>
            </View>    
            <View style = {local.phoneContainer}>
                <Text>{phone}</Text>
            </View>
        </View>
        </TouchableOpacity>
    )
}

const CustomList = ({navigation}) => {
    return (
      <FlatList
        data={DATA}
        renderItem={({item}) => (
          <ItemList name={item.name} phone={item.phone} navigation={navigation}/>
        )}
      />
    );
  };

const local = StyleSheet.create({
    itemContainer: {
        margin: 15,
        borderWidth : 1,
        padding: 5,
        borderRadius: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    identityContainer: {},
    phoneContainer: {},
})

export default CustomList;