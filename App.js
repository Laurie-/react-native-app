import React from 'react';
import CustomList from './src/components/CustomList';
import { NavigationContainer } from '@react-navigation/native';
import StackNavigator from './src/navigation/stackNavigator';
//import MainScreen from './src/screens/MainScreen';

const App = () => {
  return (
    <NavigationContainer initialRouteName="Home">
      <StackNavigator/>
    </NavigationContainer>
  );
};

export default App;